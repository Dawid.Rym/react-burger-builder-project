import React, { Component } from 'react';

import Aux from '../../hoc/Aux';
import Burger from '../../components/Burger/Burger';
import BuildControls from '../../components/Burger/BuildControls/BuildControls';
import Modal from '../../components/UI/Modal/Modal'
import OrderSummary from '../../components/Burger/OrderSummary/OrderSummary'
const INGREDIENT_PRICES = {
    salad: 0.5,
    cheese: 0.4,
    meat: 1.3,
    bacon: 0.7
};

class BurgerBuilder extends Component {
    // constructor(props) {
    //     super(props);
    //     this.state = {...}
    // }
    state = {
        ingredients: {
            salad: 0,
            bacon: 0,
            cheese: 0,
            meat: 0
        },
        totalPrice: 4,
        purchasable: false,
        orderClicked: false
    }

    updatePurchaseState (ingredients) {
        // ['meat', 'bacon' itd]
        const sum = Object.keys( ingredients )
            .map( igKey => {
                return ingredients[igKey]; // ['0', '0', '0']
            } )
            .reduce( ( sum, el ) => { //sum jest to accumulator do argument do ktrego bedziemy sumowac , a el to elemnty do sumowania
                return sum + el;
            },0  );
        this.setState( { purchasable: sum > 0 } );
    }

    addIngredientHandler = ( type ) => {
        const oldCount = this.state.ingredients[type];
        const updatedCount = oldCount + 1;
        const updatedIngredients = {
            ...this.state.ingredients
        };
        updatedIngredients[type] = updatedCount;
        const priceAddition = INGREDIENT_PRICES[type];
        const oldPrice = this.state.totalPrice;
        const newPrice = oldPrice + priceAddition;
        this.setState( { totalPrice: newPrice, ingredients: updatedIngredients } );
        this.updatePurchaseState(updatedIngredients);
    }

    removeIngredientHandler = ( type ) => {
        const oldCount = this.state.ingredients[type];
        if ( oldCount <= 0 ) {
            return;
        }
        const updatedCount = oldCount - 1;
        const updatedIngredients = {
            ...this.state.ingredients
        };
        updatedIngredients[type] = updatedCount;
        const priceDeduction = INGREDIENT_PRICES[type];
        const oldPrice = this.state.totalPrice;
        const newPrice = oldPrice - priceDeduction;
        this.setState( { totalPrice: newPrice, ingredients: updatedIngredients } );
        this.updatePurchaseState(updatedIngredients);
    }
    orderButtonClicked = ()=>{
        this.setState({
            orderClicked: true
        }) 
    }
    modalCancelHandler = () =>{
        this.setState({
            orderClicked: false
        })
    }
    modalCountinueHandler = () =>{
       alert('You are continuing ! ')
    }

     render () {
        const disabledInfo = {
            ...this.state.ingredients
        };
        for ( let key in disabledInfo ) {
            disabledInfo[key] = disabledInfo[key] <= 0
        }
        // {salad: true, meat: false, ...}
        
        return (
            <Aux>
                <Modal show ={this.state.orderClicked} modalClosed = {this.modalCancelHandler}>
                    <OrderSummary ingredinets = {this.state.ingredients}
                    price = {this.state.totalPrice}
                    modalContinue = {this.modalCountinueHandler}
                    modalClosed = {this.modalCancelHandler}
                    />
                </Modal>
                <Burger ingredients={this.state.ingredients} />
                <BuildControls
                    ingredientAdded={this.addIngredientHandler}
                    ingredientRemoved={this.removeIngredientHandler}
                    disabled={disabledInfo}
                    purchasable={this.state.purchasable}
                    clicked ={this.orderButtonClicked}
                    price={this.state.totalPrice} />
                
            </Aux>
        );
    }
}

export default BurgerBuilder;