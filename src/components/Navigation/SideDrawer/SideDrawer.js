import React from 'react'
import Logo from '../.././Logo/Logo'
import NavigationItems from '../NavigationItems/NavigationItems'
import styles from './SideDrawer.css'
import SideDrawerBackdrop from '../../UI/Backdrop/SideDrawerBackdrop'
import Aux from '../../../hoc/Aux'
const sideDrawer = (props)=>{
    let assignedStyles = [styles.SideDrawer, styles.Close]
    if(props.open === true){
     assignedStyles = [styles.SideDrawer, styles.Open]
    }
    return(
        <Aux>
            <SideDrawerBackdrop show={props.open} clicked = {props.closed}/>
            
        <div className = {assignedStyles.join(' ')}>
        <div className = {styles.Logo}>
            <Logo/>
        </div>
            <nav>
                <NavigationItems/>
            </nav>
        </div>
        </Aux>
    )
}



export default sideDrawer