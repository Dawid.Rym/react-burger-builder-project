import React from 'react'
import styles from './Backdrop.css'
const backdrop = (props) =>(
    props.show === true ? <div onClick={props.backdropClicked} className = {styles.Backdrop}></div> : null
)


export default backdrop
