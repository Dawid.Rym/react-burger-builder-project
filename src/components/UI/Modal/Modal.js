import React, {Component} from 'react'
import styles from './Modal.css'
import Aux from '../../../hoc/Aux'
import Backdrop from '../Backdrop/Backdrop'



class Modal extends Component{
   shouldComponentUpdate(nextProps,nextState){
       return nextProps.show !== this.props.show
          
       
   }
    componentWillUpdate(){
        console.log('[Modal] will update')
    }
    render(){
        return(
            <Aux>
                    <Backdrop show = {this.props.show} backdropClicked = {this.props.modalClosed}/>
                <div  
                className = {styles.Modal}
                style = {{
                    transform: this.props.show===true  ? 'translateY(0)' : 'translateY(-100vh)', opacity: this.props.show ===true ? '1' : '0' }}>
                    
                    {this.props.children}
                </div>
             </Aux>
        )
    }
} 

   


export default Modal